use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gdk, gio, glib};
use log::{debug, info};

use crate::config::{APP_ID, PKGDATADIR, PROFILE, VERSION};
use crate::window::ThirstApplicationWindow;

mod imp {
    use super::*;
    use adw::subclass::prelude::AdwApplicationImpl;
    use glib::WeakRef;
    use once_cell::sync::OnceCell;

    #[derive(Debug, Default)]
    pub struct ThirstApplication {
        pub window: OnceCell<WeakRef<ThirstApplicationWindow>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ThirstApplication {
        const NAME: &'static str = "ThirstApplication";
        type Type = super::ThirstApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for ThirstApplication {}

    impl ApplicationImpl for ThirstApplication {
        fn activate(&self, app: &Self::Type) {
            debug!("GtkApplication<ThirstApplication>::activate");

            if let Some(window) = self.window.get() {
                let window = window.upgrade().unwrap();
                window.show();
                window.present();
                return;
            }

            let window = ThirstApplicationWindow::new(app);
            self.window
                .set(window.downgrade())
                .expect("Window already set.");

            app.main_window().present();
        }

        fn startup(&self, app: &Self::Type) {
            debug!("GtkApplication<ThirstApplication>::startup");
            self.parent_startup(app);

            // Set icons for shell
            gtk::Window::set_default_icon_name(APP_ID);

            app.setup_css();
            app.setup_gactions();
            app.setup_accels();
        }
    }

    impl GtkApplicationImpl for ThirstApplication {}
    impl AdwApplicationImpl for ThirstApplication {}
}

glib::wrapper! {
    pub struct ThirstApplication(ObjectSubclass<imp::ThirstApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl ThirstApplication {
    pub fn new() -> Self {
        glib::Object::new(&[
            ("application-id", &Some(APP_ID)),
            ("flags", &gio::ApplicationFlags::empty()),
            ("resource-base-path", &Some("/com/gitlab/Thirst/")),
        ])
        .expect("Application initialization failed...")
    }

    fn main_window(&self) -> ThirstApplicationWindow {
        let imp = imp::ThirstApplication::from_instance(self);
        imp.window.get().unwrap().upgrade().unwrap()
    }

    fn setup_gactions(&self) {
        // Quit
        let action_quit = gio::SimpleAction::new("quit", None);
        action_quit.connect_activate(clone!(@weak self as app => move |_, _| {
            // This is needed to trigger the delete event and saving the window state
            app.main_window().close();
            app.quit();
        }));
        self.add_action(&action_quit);

        // About
        let action_about = gio::SimpleAction::new("about", None);
        action_about.connect_activate(clone!(@weak self as app => move |_, _| {
            app.show_about_dialog();
        }));
        self.add_action(&action_about);
    }

    // Sets up keyboard shortcuts
    fn setup_accels(&self) {
        self.set_accels_for_action("app.quit", &["<primary>q"]);
    }

    fn setup_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/com/gitlab/Thirst/style.css");
        if let Some(display) = gdk::Display::default() {
            gtk::StyleContext::add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }

    fn show_about_dialog(&self) {
        let dialog = gtk::AboutDialogBuilder::new()
            .program_name("Thirst")
            .logo_icon_name(APP_ID)
            .license_type(gtk::License::MitX11)
            .website("https://gitlab.com/Jeriel/thirst")
            .version(VERSION)
            .transient_for(&self.main_window())
            .modal(true)
            .authors(vec!["Jeriel Baptista Verissimo".into()])
            .artists(vec!["Jeriel Baptista Verissimo".into()])
            .build();

        dialog.show();
    }

    pub fn run(&self) {
        info!("Thirst ({})", APP_ID);
        info!("Version: {} ({})", VERSION, PROFILE);
        info!("Datadir: {}", PKGDATADIR);

        ApplicationExtManual::run(self);
    }
}
